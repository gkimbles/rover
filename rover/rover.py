from typing import List

from rover.surface import Surface


class Rover:
    def __init__(self, x_coord: int, y_coord: int, orientation: str, surface: Surface):
        self.x_coord = x_coord
        self.y_coord = y_coord
        self.orientation = orientation
        self.surface = surface

    def process_commands(self, commands: List):
        for command in commands:
            try:
                if command in self.commands.keys():
                    self._process_command(command)
                else:
                    raise ValueError(f"Invalid argument: {command}")
            except MovementException as e:
                self.report(f"{e.message} Halting at {self.x_coord}, {self.y_coord}")
                break

    def _process_command(self, command):
        movement_method = self.commands[command]
        movement_method(self)

    def _move_forward(self):
        next_x_coord = self.x_coord + self.position_effects[self.orientation]['x_change']
        next_y_coord = self.y_coord + self.position_effects[self.orientation]['y_change']
        self._move_and_check(next_x_coord, next_y_coord)

    def _move_backward(self):
        next_x_coord = self.x_coord - self.position_effects[self.orientation]['x_change']
        next_y_coord = self.y_coord - self.position_effects[self.orientation]['y_change']
        self._move_and_check(next_x_coord, next_y_coord)

    def _move_and_check(self, next_x_coord: int, next_y_coord: int):
        if self.surface.check_obstacle(next_x_coord, next_y_coord):
            raise MovementException(next_x_coord, next_y_coord)
        self.x_coord, self.y_coord = self.surface.wrap_coordinates(next_x_coord, next_y_coord)

    def _turn_right(self):
        self.orientation = self.position_effects[self.orientation]['right']

    def _turn_left(self):
        self.orientation = self.position_effects[self.orientation]['left']

    commands = {
        0: _move_forward,
        1: _move_backward,
        2: _turn_left,
        3: _turn_right,
    }

    position_effects = {
        'N': {'x_change': 0, 'y_change': 1, 'left': 'W', 'right': 'E'},
        'E': {'x_change': 1, 'y_change': 0, 'left': 'N', 'right': 'S'},
        'S': {'x_change': 0, 'y_change': -1, 'left': 'E', 'right': 'W'},
        'W': {'x_change': -1, 'y_change': 0, 'left': 'S', 'right': 'N'},
    }

    def report(self, message):
        print(message)


class MovementException(Exception):
    def __init__(self, obstacle_location_x, obstacle_location_y):
        self.message = f"Attempted to move into obstacle at {obstacle_location_x}, {obstacle_location_y}."
        super().__init__(self.message)
