from typing import List, Tuple


class Surface:
    def __init__(self, x_size, y_size):
        self.x_size = x_size
        self.y_size = y_size
        self.obstacles = set()

    def wrap_coordinates(self, x_coordinate: int, y_coordinate: int):
        return x_coordinate % self.x_size, y_coordinate % self.y_size

    def load_obstacles(self, obstacles: List[Tuple[int, int]]):
        for obstacle in obstacles:
            self.obstacles.add(obstacle)

    def check_obstacle(self, x_coord: int, y_coord: int):
        if (x_coord, y_coord) in self.obstacles:
            return True
        return False
