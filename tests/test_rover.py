import pytest

from rover.rover import Rover, MovementException
from rover.surface import Surface


class TestRoverInputs:
    def test_rover_processes_input_list(self):
        surface = Surface(10, 10)
        sut = Rover(1, 2, 'N', surface)
        # Move forwards twice, turn right, reverse, turn left twice
        commands = [0, 3, 1, 2]
        sut.process_commands(commands)

    def test_rover_throws_exception_on_invalid_input(self):
        with pytest.raises(ValueError):
            grid = Surface(10, 10)
            sut = Rover(1, 2, 'N', grid)
            # Move forwards twice, turn right, reverse, turn left twice
            commands = [0, 'X']
            sut.process_commands(commands)

    def test_rover_handles_integer_array(self):
        surface = Surface(10, 10)
        sut = Rover(1, 2, 'N', surface)
        # Move forwards twice, turn right, reverse, turn left twice
        commands = [0, 1, 2, 3]
        sut.process_commands(commands)


class TestRoverMovement:
    @pytest.mark.parametrize("starting_orientation, ending_orientation",
                             [("N", "W"), ("W", "S"), ("S", "E"), ("E", "N")])
    def test_rover_can_turn_left(self, starting_orientation, ending_orientation):
        sut = Rover(1, 1, starting_orientation, Surface(10, 10))
        # Move forwards twice, turn right, reverse, turn left twice
        sut.process_commands([2])
        assert sut.orientation == ending_orientation

    @pytest.mark.parametrize("starting_orientation, ending_orientation",
                             [("N", "E"), ("W", "N"), ("S", "W"), ("E", "S")])
    def test_rover_can_turn_right(self, starting_orientation, ending_orientation):
        sut = Rover(1, 1, starting_orientation, Surface(10, 10))
        # Move forwards twice, turn right, reverse, turn left twice
        sut.process_commands([3])
        assert sut.orientation == ending_orientation

    def test_rover_follows_path(self):
        sut = Rover(1, 1, 'N', Surface(10, 10))
        sut.process_commands([0, 0, 0, 3, 1])
        assert sut.x_coord == 0
        assert sut.y_coord == 4

    def test_rover_coordinates_wrap_around_grid_from_top_and_bottom(self):
        sut = Rover(1, 1, 'N', Surface(2, 2))
        sut.process_commands([0])
        assert sut.y_coord == 0
        sut.process_commands([1])
        assert sut.y_coord == 1

    def test_rover_coordinates_wrap_around_grid_from_sides(self):
        sut = Rover(1, 1, 'E', Surface(2, 2))
        sut.process_commands([0])
        assert sut.x_coord == 0
        sut.process_commands([1])
        assert sut.y_coord == 1

    @pytest.mark.parametrize("starting_orientation, message",
                             [("N", "Attempted to move into obstacle at 1, 2. Halting at 1, 1\n"),
                              ("E", "Attempted to move into obstacle at 2, 1. Halting at 1, 1\n"),
                              ("S", "Attempted to move into obstacle at 1, 0. Halting at 1, 1\n"),
                              ("W", "Attempted to move into obstacle at 0, 1. Halting at 1, 1\n")
                              ])
    def test_rover_halts_at_obstacle_going_forwards(self, capsys, starting_orientation, message):
        surface = Surface(3, 3)
        surface.load_obstacles([(1, 0), (0, 1), (2, 1), (1, 2)])
        sut = Rover(1, 1, starting_orientation, surface)
        sut.process_commands([0])
        captured = capsys.readouterr()
        assert captured.out == message
        assert sut.y_coord == 1
        assert sut.x_coord == 1

    @pytest.mark.parametrize("starting_orientation, message",
                             [("N", "Attempted to move into obstacle at 1, 0. Halting at 1, 1\n"),
                              ("E", "Attempted to move into obstacle at 0, 1. Halting at 1, 1\n"),
                              ("S", "Attempted to move into obstacle at 1, 2. Halting at 1, 1\n"),
                              ("W", "Attempted to move into obstacle at 2, 1. Halting at 1, 1\n")
                              ])
    def test_rover_halts_at_obstacle_going_backwards(self, capsys, starting_orientation, message):
        surface = Surface(3, 3)
        surface.load_obstacles([(1, 0), (0, 1), (2, 1), (1, 2)])
        sut = Rover(1, 1, starting_orientation, surface)
        sut.process_commands([1])
        captured = capsys.readouterr()
        assert captured.out == message
        assert sut.y_coord == 1
        assert sut.x_coord == 1

    def test_rover_halts_at_obstacle(self):
        surface = Surface(3, 3)
        surface.load_obstacles([(2, 1), (1, 2)])
        sut = Rover(1, 1, 'N', surface)
        sut.process_commands([0, 1])
        assert sut.y_coord == 1
        assert sut.x_coord == 1
