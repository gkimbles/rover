from rover.surface import Surface


class TestSurface:
    def test_wrap_coordinates_returns_expected_coordinates(self):
        # Create a 2x2 surface (indices max at 1, 1)
        sut = Surface(2, 2)
        x, y = sut.wrap_coordinates(2, 2)
        assert x == 0
        assert y == 0

    def test_load_obstacles(self):
        sut = Surface(2, 2)
        obstacles = [(0, 0), (1, 1)]
        sut.load_obstacles(obstacles)

    def test_check_obstacles(self):
        sut = Surface(2, 2)
        obstacles = [(0, 0), (1, 1)]
        sut.load_obstacles(obstacles)
        assert sut.check_obstacle(0, 0)
        assert not sut.check_obstacle(0, 1)
