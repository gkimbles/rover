# Rover
A simple library to control a Rover on a planet.

## A simple example

```python
from rover.rover import Rover
from rover.surface import Surface

planet = Surface(10, 10)
rover = Rover(0, 0, 'E', planet)
commands = [0, 2, 0, 0]

rover.process_commands(commands)
```